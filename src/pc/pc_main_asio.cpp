#include <iostream>
#include <memory>
#include <cstdint>
#include <cctype>
#include <boost/asio.hpp>
#include <cdlpp/cdltypes.hpp>

extern "C" {
#include "sm64.h"
#include "game/print.h"
#include "game/area.h"
#include "PR/gbi.h"
#include "game/segment2.h"
#include "game/game_init.h"
#include "game/hud.h"
#include "game/ingame_menu.h"
#include "level_commands.h"
#include "game/level_update.h"

#include "game/memory.h"
#include "audio/external.h"

#include "gfx/gfx_pc.h"
#include "gfx/gfx_opengl.h"
#include "gfx/gfx_direct3d11.h"
#include "gfx/gfx_direct3d12.h"
#include "gfx/gfx_dxgi.h"
#include "gfx/gfx_glx.h"
#include "gfx/gfx_sdl.h"
#include "gfx/gfx_dummy.h"

extern struct GfxWindowManagerAPI *wm_api;
void produce_one_frame(void);
void gfx_run(Gfx *commands);

static const auto loopSpeed = boost::asio::chrono::milliseconds(30);
static std::shared_ptr<boost::asio::io_context> asioctx;
static boost::asio::steady_timer *t = nullptr;
static std::string curr_hudtxt;


static char charOK(char character) {
    character = std::tolower(character);
    switch (character) {
    case 'a':
    case 'b':
    case 'c':
    case 'd':
    case 'e':
    case 'f':
    case 'g':
    case 'h':
    case 'i':
    case 'k':
    case 'l':
    case 'm':
    case 'n':
    case 'o':
    case 'p':
    case 'r':
    case 's':
    case 't':
    case 'u':
    case 'w':
    case 'y':
    case '0':
    case '1':
    case '2':
    case '3':
    case '4':
    case '5':
    case '6':
    case '7':
    case '8':
    case '9':
    case '+':
    case '-':
    case ',':
        return character;
    case 'x':
        return '*';
    default:
        return ' ';
    }
}

static void gameKickoff(const boost::system::error_code&) {
    // Recreate timer
    if (t) delete t;
    t = new boost::asio::steady_timer(*asioctx, loopSpeed);

    // Print to HUD
    if (not curr_hudtxt.empty()) {
        print_text(5, 5, curr_hudtxt.c_str());
    }
    
    // Produce a frame
    produce_one_frame();
    
    // Rerun timer
    t->async_wait(gameKickoff);
}

namespace commands {
    using namespace CDL;

    void hudtxt(CMessage msg, CChannel channel, cmdargs& args) {
        if (args.empty()) {
            curr_hudtxt = "";
        } else {
            curr_hudtxt = args[0];
            for (auto& character : curr_hudtxt) {
                character = charOK(character);
            }
        }
    }
}

void mainpploop() {
    asioctx = std::make_shared<boost::asio::io_context>();
    
    // Start game
    gameKickoff(boost::system::error_code());
    
    // Initialise bot
    using namespace CDL;
    register_command("hudtxt", commands::hudtxt, 1);
    handlers::get_prefix = [] (CChannel, std::function<void (const std::string&)> cb) {
        cb("sm64");
    };
    using namespace intent_vals;
    CDL::main(0, {nullptr}, GUILD_MESSAGES | DIRECT_MESSAGES, asioctx);
}
}
